package com.fhms.akka.routing.strategies.smallestmailbox;

import java.io.IOException;

import com.fhms.akka.routing.actors.CalculatorActor;
import com.fhms.akka.routing.actors.CalculatorCallerActor;
import com.fhms.akka.routing.helper.ConsoleHelper;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.routing.FromConfig;
import akka.routing.SmallestMailboxPool;

public class SmallestMailboxPoolShowcase {
	public static void main(String[] args) throws IOException {
		ActorSystem system = ActorSystem.create("SmallestMailboxPoolSystem");
		try {
			/*
			 * There is no Group variant of the SmallestMailboxPool because the size of the
			 * mailbox and the internal dispatching state of the actor is not practically
			 * available from the paths of the routees.
			 */

			ActorRef callerActor = system.actorOf(CalculatorCallerActor.props(), "CalculatorCaller");

			// 1. Option: Initialize by code
			ActorRef smallestMailboxRouter1 = system.actorOf(
					new SmallestMailboxPool(3).props(CalculatorActor.props()), "smallest-mailbox-pool-router-code");

			for (int i = 1; i <= 100; i++) {
				CalculatorActor.AddMessage addMessage = new CalculatorActor.AddMessage(i, i);

				smallestMailboxRouter1.tell(addMessage, callerActor);
			}
			
			ConsoleHelper.waitForEnter();

			callerActor.tell(new CalculatorCallerActor.PrintMessage(), ActorRef.noSender());

			// 2. Option: Initialize by configuration
			ActorRef smallestMailboxRouter2 = system.actorOf(
					FromConfig.getInstance().props(CalculatorActor.props()), "smallest-mailbox-pool-router");

			for (int i = 1; i <= 6; i++) {
				CalculatorActor.AddMessage addMessage = new CalculatorActor.AddMessage(i, i);

				smallestMailboxRouter2.tell(addMessage, callerActor);
			}
			
			ConsoleHelper.waitForEnter();

			callerActor.tell(new CalculatorCallerActor.PrintMessage(), ActorRef.noSender());
		} finally {
			system.terminate();
		}
	}
}
