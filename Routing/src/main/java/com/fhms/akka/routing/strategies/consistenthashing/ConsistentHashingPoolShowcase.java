package com.fhms.akka.routing.strategies.consistenthashing;

import com.fhms.akka.routing.helper.ConsoleHelper;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.routing.ConsistentHashingPool;
import akka.routing.ConsistentHashingRouter;
import akka.routing.FromConfig;

public class ConsistentHashingPoolShowcase {

	public static void main(String[] args) {
		ActorSystem system = ActorSystem.create("ConsistentHashingPoolSystem");
		try {
			ActorRef caller = system.actorOf(CacheCallerActor.props(), "cacheCaller");

			// 1. Option: Initialize by code
			ActorRef router1 = system.actorOf(new ConsistentHashingPool(10).props(Cache.props()), "cache");

			// Messages implement ConsistentHashable-Interface
			router1.tell(new HashableEntry("Aalst", "A"), caller);
			router1.tell(new HashableEntry("Aalst", "A"), caller);

			router1.tell(new HashableEntry("Mustermann", "M"), caller);
			router1.tell(new HashableEntry("Mustermann", "M"), caller);

			router1.tell(new HashableEntry("Züwerink", "Z"), caller);
			router1.tell(new HashableEntry("Züwerink", "Z"), caller);

			// Message wrapped in envelope
			router1.tell(new ConsistentHashingRouter.ConsistentHashableEnvelope(new Entry("Züwerink", "Z"), "Züwerink"),
					caller);

			// 2. Option: Initialize by configuration
			ActorRef router2 = system.actorOf(FromConfig.getInstance().props(Cache.props()),
					"consistent-hashing-pool-router");

			// Messages implement ConsistentHashable-Interface
			router2.tell(new HashableEntry("Aalst", "A"), caller);
			router2.tell(new HashableEntry("Aalst", "A"), caller);

			router2.tell(new HashableEntry("Mustermann", "M"), caller);
			router2.tell(new HashableEntry("Mustermann", "M"), caller);

			router2.tell(new HashableEntry("Züwerink", "Z"), caller);
			router2.tell(new HashableEntry("Züwerink", "Z"), caller);

			// Message wrapped in envelope
			router2.tell(new ConsistentHashingRouter.ConsistentHashableEnvelope(new Entry("Züwerink", "Z"), "Züwerink"),
					caller);

			ConsoleHelper.waitForEnter();
		} finally {
			system.terminate();
		}
	}

}
