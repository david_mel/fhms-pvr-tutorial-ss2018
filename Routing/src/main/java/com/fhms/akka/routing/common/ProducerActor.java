package com.fhms.akka.routing.common;

import akka.actor.AbstractLoggingActor;
import akka.actor.ActorRef;
import akka.actor.Props;

public class ProducerActor extends AbstractLoggingActor {
    public static final class Tick {    }

    private final ActorRef _router;

    public ProducerActor(ActorRef router){
        _router = router;
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(ProducerActor.Tick.class, this::onMessage)
                .build();
    }

    private void onMessage(ProducerActor.Tick message) {
        _router.tell(new ConsumerActor.Produce("Musikanlage"), getSender());
    }

    public static Props props(ActorRef router) {
        return Props.create(ProducerActor.class, () -> new ProducerActor(router));
    }
}
