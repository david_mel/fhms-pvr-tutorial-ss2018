package com.fhms.akka.routing.actors;

import akka.actor.AbstractLoggingActor;
import akka.actor.ActorRef;
import akka.actor.Props;

public class CalculatorCallerActor extends AbstractLoggingActor {
	public static Props props() {
		return Props.create(CalculatorCallerActor.class);
	}
	
	public static class PrintMessage {}
	
	private ActorRef statisticActor;
	
	@Override
	public void preStart() throws Exception {
		super.preStart();
		
		statisticActor = getContext().actorOf(StatisticActor.props());
	}

	@Override
	public Receive createReceive() {
		return receiveBuilder()
				.match(CalculatorActor.ResultMessage.class, this::onMessage)
				.match(CalculatorCallerActor.PrintMessage.class, this::onMessage)
				.build();
	}

	private void onMessage(CalculatorActor.ResultMessage msg) {
		log().info("Result {} from {}", msg.getResult(), msg.getActorName());

		String actorName = msg.getActorName();

		statisticActor.tell(new StatisticActor.StatMessage(actorName), getSelf());
	}
	private void onMessage(CalculatorCallerActor.PrintMessage msg) {
		statisticActor.tell(new StatisticActor.PrintMessage(), getSelf());
	}
}
