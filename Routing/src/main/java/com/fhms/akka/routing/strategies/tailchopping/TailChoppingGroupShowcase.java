package com.fhms.akka.routing.strategies.tailchopping;

import java.io.IOException;
import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.fhms.akka.routing.helper.ConsoleHelper;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.routing.FromConfig;
import akka.routing.TailChoppingGroup;
import scala.concurrent.duration.FiniteDuration;

public class TailChoppingGroupShowcase {
	public static void main(String[] args) throws IOException {
		ActorSystem system = ActorSystem.create("BalancingPoolSystem");
		try {
			ActorRef callerActor = system.actorOf(TailChoppingCallerActor.props(), "caller");			

			// Create actors manually
			system.actorOf(TailChoppingCalculatorActor.props(), "calculator1");
			system.actorOf(TailChoppingCalculatorActor.props(), "calculator2");
			system.actorOf(TailChoppingCalculatorActor.props(), "calculator3");

			FiniteDuration within3 = FiniteDuration.create(5, TimeUnit.SECONDS);
			FiniteDuration interval = FiniteDuration.create(500, TimeUnit.MILLISECONDS);

			// 1. Option: Initialize by code
			List<String> paths = Arrays.asList("/user/calculator1", "/user/calculator2", "/user/calculator3");
			ActorRef tailChoppingRouter1 = system.actorOf(
					new TailChoppingGroup(paths, within3, interval).props(),
					"tail-chopping-group-router-code");

			TailChoppingCalculatorActor.AddMessage addMessage = new TailChoppingCalculatorActor.AddMessage(2, 4);

			system.scheduler().scheduleOnce(Duration.ZERO, tailChoppingRouter1, addMessage, system.dispatcher(),
					callerActor);

			ConsoleHelper.waitForEnter();

			// 2. Option: Initialize by configuration
			ActorRef tailChoppingRouter2 = system.actorOf(FromConfig.getInstance().props(TailChoppingCalculatorActor.props()),
					"tail-chopping-group-router");

			system.scheduler().scheduleOnce(Duration.ZERO, tailChoppingRouter2, addMessage, system.dispatcher(),
					callerActor);

			ConsoleHelper.waitForEnter();
		} finally {
			system.terminate();
		}
	}
}
