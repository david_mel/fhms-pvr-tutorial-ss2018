package com.fhms.akka.routing.actors;

import java.util.HashMap;
import java.util.concurrent.atomic.AtomicInteger;

import akka.actor.AbstractLoggingActor;
import akka.actor.Props;

public class StatisticActor extends AbstractLoggingActor {
	
	private HashMap<String, AtomicInteger> _map = new HashMap<>();
	
	public static class PrintMessage {}
	public static class StatMessage {
		private final String actorName;

		public StatMessage(String actorName) {
			super();
			this.actorName = actorName;
		}

		public String getActorName() {
			return actorName;
		}		
	}
	
	public static Props props() {
		return Props.create(StatisticActor.class);
	}
	
	@Override
	public Receive createReceive() {
		return receiveBuilder()
				.match(PrintMessage.class, this::onMessage)
				.match(StatMessage.class, this::onMessage)
				.build();
	}

	private void onMessage(PrintMessage msg) {
		int total = 0;
		
		for (AtomicInteger item : _map.values()) {
			total += item.get();
		}
		
		log().info("");
		for (String key : _map.keySet()) {
			int current = _map.get(key).get();
			
			log().info("Stats#  {}: {}  {}%", key, current, Math.round((double)current/(double)total * 100.0));	
		}
		log().info("");
		
		_map.clear();
	}
	private void onMessage(StatMessage msg) {
		if(!_map.containsKey(msg.actorName)) {
			_map.put(msg.actorName, new AtomicInteger(1));
		}
		else {
			_map.get(msg.actorName).incrementAndGet();
		}
	}
}
