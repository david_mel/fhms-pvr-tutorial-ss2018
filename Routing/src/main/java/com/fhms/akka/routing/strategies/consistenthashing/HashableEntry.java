package com.fhms.akka.routing.strategies.consistenthashing;

import java.io.Serializable;

import akka.routing.ConsistentHashingRouter.ConsistentHashable;

public final class HashableEntry implements Serializable, ConsistentHashable {
	private static final long serialVersionUID = 1L;
	public final String key;
	public final String value;

	public HashableEntry(String key, String value) {
		this.key = key;
		this.value = value;
	}

	public Object consistentHashKey() {
		return key;
	}
}
