package com.fhms.akka.routing.strategies.consistenthashing;

import java.util.Arrays;
import java.util.List;

import com.fhms.akka.routing.helper.ConsoleHelper;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.routing.ConsistentHashingGroup;
import akka.routing.ConsistentHashingRouter;
import akka.routing.FromConfig;

public class ConsistentHashingGroupShowcase {

	public static void main(String[] args) {
		ActorSystem system = ActorSystem.create("ConsistentHashingGroupSystem");
		try {
			ActorRef caller = system.actorOf(CacheCallerActor.props(), "cacheCaller");
			
			// Create actors manually
			system.actorOf(Cache.props(), "cache1");
			system.actorOf(Cache.props(), "cache2");
			system.actorOf(Cache.props(), "cache3");
			system.actorOf(Cache.props(), "cache4");
			system.actorOf(Cache.props(), "cache5");

			// 1. Option: Initialize by code
			List<String> paths = Arrays.asList("/user/cache1", "/user/cache2", "/user/cache3", "/user/cache4", "/user/cache5");
			ActorRef router1 = system.actorOf(new ConsistentHashingGroup(paths).props(), "cache");

			// Messages implement ConsistentHashable-Interface
			router1.tell(new HashableEntry("Aalst", "A"), caller);
			router1.tell(new HashableEntry("Aalst", "A"), caller);

			router1.tell(new HashableEntry("Mustermann", "M"), caller);
			router1.tell(new HashableEntry("Mustermann", "M"), caller);

			router1.tell(new HashableEntry("Züwerink", "Z"), caller);
			router1.tell(new HashableEntry("Züwerink", "Z"), caller);

			// Message wrapped in envelope
			router1.tell(new ConsistentHashingRouter.ConsistentHashableEnvelope(new Entry("Züwerink", "Z"), "Züwerink"),
					caller);

			// 2. Option: Initialize by configuration
			ActorRef router2 = system.actorOf(FromConfig.getInstance().props(Cache.props()),
					"consistent-hashing-group-router");

			// Messages implement ConsistentHashable-Interface
			router2.tell(new HashableEntry("Aalst", "A"), caller);
			router2.tell(new HashableEntry("Aalst", "A"), caller);

			router2.tell(new HashableEntry("Mustermann", "M"), caller);
			router2.tell(new HashableEntry("Mustermann", "M"), caller);

			router2.tell(new HashableEntry("Züwerink", "Z"), caller);
			router2.tell(new HashableEntry("Züwerink", "Z"), caller);

			// Message wrapped in envelope
			router2.tell(new ConsistentHashingRouter.ConsistentHashableEnvelope(new Entry("Züwerink", "Z"), "Züwerink"),
					caller);

			ConsoleHelper.waitForEnter();
		} finally {
			system.terminate();
		}
	}

}
