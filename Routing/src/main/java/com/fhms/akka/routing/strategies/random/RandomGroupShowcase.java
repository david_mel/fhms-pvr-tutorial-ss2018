package com.fhms.akka.routing.strategies.random;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import com.fhms.akka.routing.actors.CalculatorActor;
import com.fhms.akka.routing.actors.CalculatorCallerActor;
import com.fhms.akka.routing.helper.ConsoleHelper;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.routing.FromConfig;
import akka.routing.RoundRobinGroup;

public class RandomGroupShowcase {
    public static void main(String[] args) throws IOException {
        ActorSystem system = ActorSystem.create("RandomGroupSystem");
        try {
        	ActorRef callerActor = system.actorOf(CalculatorCallerActor.props(), "CalculatorCaller");

			// Create actors manually
			system.actorOf(CalculatorActor.props(), "calculator1");
			system.actorOf(CalculatorActor.props(), "calculator2");
			system.actorOf(CalculatorActor.props(), "calculator3");

			// 1. Option: Initialize by code
			List<String> paths = Arrays.asList("/user/calculator1", "/user/calculator2", "/user/calculator3");
			ActorRef router1 = system.actorOf(new RoundRobinGroup(paths).props(), "random-group-router-code");

			for (int i = 1; i <= 6; i++) {
				CalculatorActor.AddMessage addMessage = new CalculatorActor.AddMessage(i, i);

				router1.tell(addMessage, callerActor);
			}
			
			ConsoleHelper.waitForEnter();
			
			callerActor.tell(new CalculatorCallerActor.PrintMessage(), ActorRef.noSender());

			// 2. Option: Initialize by configuration
			ActorRef router2 = system.actorOf(FromConfig.getInstance().props(), "random-group-router");

			for (int i = 1; i <= 6; i++) {
				CalculatorActor.AddMessage addMessage = new CalculatorActor.AddMessage(i, i);

				router2.tell(addMessage, callerActor);
			}

			ConsoleHelper.waitForEnter();
			
			callerActor.tell(new CalculatorCallerActor.PrintMessage(), ActorRef.noSender());
        } finally {
            system.terminate();
        }
    }
}
