package com.fhms.akka.routing.strategies.balancingpool;

import java.io.IOException;

import com.fhms.akka.routing.actors.CalculatorActor;
import com.fhms.akka.routing.actors.CalculatorCallerActor;
import com.fhms.akka.routing.helper.ConsoleHelper;
import com.fhms.akka.routing.helper.ProduceHelper;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.routing.BalancingPool;
import akka.routing.FromConfig;

public class BalancingPoolShowcase {
	public static void main(String[] args) throws IOException {
		ActorSystem system = ActorSystem.create("BalancingPoolSystem");
		try {
			ActorRef callerActor = system.actorOf(CalculatorCallerActor.props(), "CalculatorCaller");

			// 1. Option: Initialize by code
			ActorRef balancingPoolRouter1 = system.actorOf(
					new BalancingPool(2).props(Props.create(CalculatorActor.class)), "balancing-pool-router-code");

			CalculatorActor.AddMessage addMessage = new CalculatorActor.AddMessage(200, 200);

			ProduceHelper.ProduceMessagesInParallel(balancingPoolRouter1, callerActor, addMessage, 4, 50);

			ConsoleHelper.waitForEnter();

			callerActor.tell(new CalculatorCallerActor.PrintMessage(), ActorRef.noSender());

			// 2. Option: Initialize by configuration
			ActorRef balancingPoolRouter2 = system.actorOf(
					FromConfig.getInstance().props(Props.create(CalculatorActor.class)), "balancing-pool-router");

			ProduceHelper.ProduceMessagesInParallel(balancingPoolRouter2, callerActor, addMessage, 4, 50);

			ConsoleHelper.waitForEnter();

			callerActor.tell(new CalculatorCallerActor.PrintMessage(), ActorRef.noSender());
		} finally {
			system.terminate();
		}
	}
}
