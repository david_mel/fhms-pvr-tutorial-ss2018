package com.fhms.akka.routing.strategies.random;

import java.io.IOException;

import com.fhms.akka.routing.actors.CalculatorActor;
import com.fhms.akka.routing.actors.CalculatorCallerActor;
import com.fhms.akka.routing.helper.ConsoleHelper;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.routing.FromConfig;
import akka.routing.RandomPool;

public class RandomPoolShowcase {
    public static void main(String[] args) throws IOException {
        ActorSystem system = ActorSystem.create("RandomPoolSystem");
        try {
            ActorRef callerActor = system.actorOf(CalculatorCallerActor.props(), "CalculatorCaller");

			// 1. Option: Initialize by code
			ActorRef router1 = system.actorOf(new RandomPool(3).props(CalculatorActor.props()),
					"random-pool-router-code");

			for (int i = 1; i <= 6; i++) {
				CalculatorActor.AddMessage addMessage = new CalculatorActor.AddMessage(i, i);

				router1.tell(addMessage, callerActor);
			}
			
			ConsoleHelper.waitForEnter();
			
			callerActor.tell(new CalculatorCallerActor.PrintMessage(), ActorRef.noSender());

			// 2. Option: Initialize by configuration
			ActorRef router2 = system.actorOf(FromConfig.getInstance().props(CalculatorActor.props()),
					"random-pool-router");

			for (int i = 1; i <= 6; i++) {
				CalculatorActor.AddMessage addMessage = new CalculatorActor.AddMessage(i, i);

				router2.tell(addMessage, callerActor);
			}
			
			ConsoleHelper.waitForEnter();
			
			callerActor.tell(new CalculatorCallerActor.PrintMessage(), ActorRef.noSender());
        } finally {
            system.terminate();
        }
    }
}
