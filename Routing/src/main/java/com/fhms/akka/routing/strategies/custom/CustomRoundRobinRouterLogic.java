package com.fhms.akka.routing.strategies.custom;

import akka.routing.Routee;
import akka.routing.RoutingLogic;
import scala.collection.immutable.IndexedSeq;

public class CustomRoundRobinRouterLogic implements RoutingLogic {

    private Integer current = 0;

    @Override
    public Routee select(Object message, IndexedSeq<Routee> routees) {   	
        Routee routee = null;
        synchronized (current) {
            routee = routees.apply(current);
            current = (current + 1) % routees.count(p -> p);
        }
        return routee;
    }
}
