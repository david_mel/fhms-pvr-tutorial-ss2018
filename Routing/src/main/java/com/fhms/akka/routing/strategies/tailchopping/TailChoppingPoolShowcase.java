package com.fhms.akka.routing.strategies.tailchopping;

import java.io.IOException;
import java.time.Duration;
import java.util.concurrent.TimeUnit;

import com.fhms.akka.routing.helper.ConsoleHelper;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;
import akka.routing.FromConfig;
import akka.routing.TailChoppingPool;
import scala.concurrent.duration.FiniteDuration;

public class TailChoppingPoolShowcase {
	public static void main(String[] args) throws IOException {
		ActorSystem system = ActorSystem.create("BalancingPoolSystem");
		try {
			ActorRef callerActor = system.actorOf(TailChoppingCallerActor.props(), "caller");

			FiniteDuration within3 = FiniteDuration.create(5, TimeUnit.SECONDS);
			FiniteDuration interval = FiniteDuration.create(500, TimeUnit.MILLISECONDS);

			// 1. Option: Initialize by code
			ActorRef tailChoppingRouter1 = system.actorOf(
					new TailChoppingPool(3, within3, interval).props(Props.create(TailChoppingCalculatorActor.class)),
					"tail-chopping-pool-router-code");

			TailChoppingCalculatorActor.AddMessage addMessage = new TailChoppingCalculatorActor.AddMessage(2, 4);

			system.scheduler().scheduleOnce(Duration.ZERO, tailChoppingRouter1, addMessage, system.dispatcher(),
					callerActor);

			ConsoleHelper.waitForEnter();

			// 2. Option: Initialize by configuration
			ActorRef tailChoppingRouter2 = system.actorOf(FromConfig.getInstance().props(TailChoppingCalculatorActor.props()),
					"tail-chopping-pool-router");

			system.scheduler().scheduleOnce(Duration.ZERO, tailChoppingRouter2, addMessage, system.dispatcher(),
					callerActor);

			ConsoleHelper.waitForEnter();
		} finally {
			system.terminate();
		}
	}
}
