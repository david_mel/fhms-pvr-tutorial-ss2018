package com.fhms.akka.routing.common;

import akka.actor.AbstractLoggingActor;
import akka.actor.Props;

public class ConsumersActor extends AbstractLoggingActor {

    private final int _numberOfConsumers;

    public ConsumersActor(int numberOfConsumers) {
        _numberOfConsumers = numberOfConsumers;
    }


    @Override
    public void preStart() throws Exception {
        super.preStart();

        for (int i = 1; i <= _numberOfConsumers; i++) {
            getContext().actorOf(ConsumerActor.props(), "c" + i);

            log().info("Consumer actor '{}/c{}' created", getSelf().path().toString(), i);
        }
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder().build();
    }

    public static Props props(int numberOfConsumers) {
        return Props.create(ConsumersActor.class, () -> new ConsumersActor(numberOfConsumers));
    }
}
