package com.fhms.akka.routing.strategies.consistenthashing;

import java.io.Serializable;

import akka.routing.ConsistentHashingRouter.ConsistentHashable;

public final class Get implements Serializable, ConsistentHashable {
	private static final long serialVersionUID = 1L;
	public final String key;

	public Get(String key) {
		this.key = key;
	}

	public Object consistentHashKey() {
		return key;
	}
}
