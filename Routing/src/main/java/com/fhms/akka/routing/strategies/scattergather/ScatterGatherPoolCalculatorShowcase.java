package com.fhms.akka.routing.strategies.scattergather;

import java.io.IOException;

import com.fhms.akka.routing.actors.CalculatorActor;
import com.fhms.akka.routing.actors.CalculatorCallerActor;
import com.fhms.akka.routing.helper.ConsoleHelper;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.routing.FromConfig;
import akka.routing.ScatterGatherFirstCompletedPool;

public class ScatterGatherPoolCalculatorShowcase {
    public static void main(String[] args) throws IOException {
        ActorSystem system = ActorSystem.create("ScatterGatherPoolSystem");
        try {
        	ActorRef callerActor = system.actorOf(CalculatorCallerActor.props(), "CalculatorCaller");
            java.time.Duration within = java.time.Duration.ofMillis(500);

            // 1. Option: Initialize by code
            ActorRef router1 = system.actorOf(
                    new ScatterGatherFirstCompletedPool(3, within).props(CalculatorActor.props()),
                    "scatter-gather-pool-router-code");
            
			for (int i = 1; i <= 6; i++) {
				CalculatorActor.AddMessage addMessage = new CalculatorActor.AddMessage(i, i);

				router1.tell(addMessage, callerActor);
			}
            
			ConsoleHelper.waitForEnter();
			
			callerActor.tell(new CalculatorCallerActor.PrintMessage(), ActorRef.noSender());
			
            // 2. Option: Initialize by configuration
            ActorRef router2 = system.actorOf(
            		FromConfig.getInstance().props(CalculatorActor.props()),
            		"scatter-gather-pool-router");

			for (int i = 1; i <= 6; i++) {
				CalculatorActor.AddMessage addMessage = new CalculatorActor.AddMessage(i, i);

				router2.tell(addMessage, callerActor);
			}

			ConsoleHelper.waitForEnter();
			
			callerActor.tell(new CalculatorCallerActor.PrintMessage(), ActorRef.noSender());
        } finally {
            system.terminate();
        }
    }
}
