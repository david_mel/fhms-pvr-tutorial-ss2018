package com.fhms.akka.routing.strategies.custom;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.routing.Router;

import java.io.IOException;
import com.fhms.akka.routing.actors.CalculatorActor;

public class CustomRoundRobinPoolShowcase {
    public static void main(String[] args) throws IOException {
        ActorSystem system = ActorSystem.create("CustomRoundRobinPoolSystem");
        try {
            Router router = new Router(new CustomRoundRobinRouterLogic());

            ActorRef calculator1 = system.actorOf(CalculatorActor.props(), "calc1");
            ActorRef calculator2 = system.actorOf(CalculatorActor.props(), "calc2");
            ActorRef calculator3 = system.actorOf(CalculatorActor.props(), "calc3");

            router.addRoutee(calculator1);
            router.addRoutee(calculator2);
            router.addRoutee(calculator3);

            CalculatorActor.AddMessage addMessage = new CalculatorActor.AddMessage(2, 2);

            router.route(addMessage, ActorRef.noSender());

            System.in.read();
        } finally {
            system.terminate();
        }
    }
}
