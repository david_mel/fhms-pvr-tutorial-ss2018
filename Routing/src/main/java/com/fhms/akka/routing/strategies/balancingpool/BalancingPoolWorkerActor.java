package com.fhms.akka.routing.strategies.balancingpool;

import java.util.concurrent.atomic.AtomicInteger;

import com.fhms.akka.routing.helper.WorkHelper;

import akka.actor.AbstractLoggingActor;

public class BalancingPoolWorkerActor extends AbstractLoggingActor {

	public static AtomicInteger Counter = new AtomicInteger(0);

	public static AtomicInteger ActorA = new AtomicInteger(0);
	public static AtomicInteger ActorB = new AtomicInteger(0);
	public static AtomicInteger ActorC = new AtomicInteger(0);

	public static final class Work {
	}

	@Override
	public Receive createReceive() {
		return receiveBuilder().match(BalancingPoolWorkerActor.Work.class, this::onMessage).build();
	}

	private void onMessage(Work work) {

		String actorName = getSelf().path().name();

		if (actorName.equals("$a")) {
			ActorA.incrementAndGet();

			WorkHelper.spin(500);
		} else if (actorName.equals("$b")) {
			ActorB.incrementAndGet();

			WorkHelper.spin(250);
		} else {
			ActorC.incrementAndGet();

			WorkHelper.spin(250);
		}

		if (Counter.incrementAndGet() == 200) {
			int total = Counter.get();
			if (ActorA.get() > 0) {
				log().info("Stats#  A: {}  {}%", ActorA.get(),
						Math.round((double) ActorA.get() / (double) total * 100.0));
			}
			if (ActorB.get() > 0) {
				log().info("Stats#  B: {}  {}%", ActorB.get(),
						Math.round((double) ActorB.get() / (double) total * 100.0));
			}
			if (ActorC.get() > 0) {
				log().info("Stats#  C: {}  {}%", ActorC.get(),
						Math.round((double) ActorC.get() / (double) total * 100.0));
			}
		}
	}
}
