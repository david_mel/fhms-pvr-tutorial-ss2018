package com.fhms.akka.routing.helper;

public class WorkHelper {
	public static void spin(int milliseconds) {
	    long sleepTime = milliseconds*1000000L;
	    long startTime = System.nanoTime();
	    while ((System.nanoTime() - startTime) < sleepTime) {}
	}
}
