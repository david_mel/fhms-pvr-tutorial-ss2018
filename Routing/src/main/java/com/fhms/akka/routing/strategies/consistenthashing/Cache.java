package com.fhms.akka.routing.strategies.consistenthashing;

import java.util.HashMap;
import java.util.Map;

import akka.actor.AbstractLoggingActor;
import akka.actor.Props;

public class Cache extends AbstractLoggingActor {

	public static Props props() {
		return Props.create(Cache.class);
	}

	Map<String, String> cache = new HashMap<String, String>();

	@Override
	public Receive createReceive() {
		return receiveBuilder()
				.match(Entry.class, this::onMessage)
				.match(HashableEntry.class, this::onMessage)
				.match(Get.class, this::onMessage)
				.build();
	}

	private void onMessage(Entry entry) {
		log().info("Received {}", entry.key);
		
		cache.put(entry.key, entry.value);
	}

	private void onMessage(HashableEntry entry) {
		log().info("Received {}", entry.key);
		
		cache.put(entry.key, entry.value);
	}

	private void onMessage(Get get) {
		log().info("Request {}", get.key);
		
		Object value = cache.get(get.key);
		getSender().tell(value == null ? "404_NOT_FOUND" : value, getSelf());
	}
}
