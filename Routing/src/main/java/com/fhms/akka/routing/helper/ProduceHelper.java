package com.fhms.akka.routing.helper;

import akka.actor.ActorRef;

public final class ProduceHelper {
    private ProduceHelper(){}

    public static void ProduceMessagesInParallel(ActorRef targetActor, ActorRef sender, Object message, int threads, int messagesPerThread){
        assert threads > 0 : "Number of threads must be a least 1.";
        assert messagesPerThread > 0 : "Number of messages per thread must be a least 1.";

        for (int i = 0; i < threads; i++) {
            new Thread(() -> {
                for (int j = 0; j < messagesPerThread; j++) {
                    targetActor.tell(message, sender);

                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
    }
}
