package com.fhms.akka.routing.common;

import akka.actor.AbstractLoggingActor;
import akka.actor.ActorRef;
import akka.actor.Props;

public class ProducerRandomWorkActor extends AbstractLoggingActor {
    public static final class Tick {    }

    private final ActorRef _router;

    public ProducerRandomWorkActor(ActorRef router){
        _router = router;
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(ProducerRandomWorkActor.Tick.class, this::onMessage)
                .match(String.class, this::onMessage)
                .build();
    }

    private void onMessage(ProducerRandomWorkActor.Tick message) {
        _router.tell(new ConsumerRandomWorkActor.Produce("Musikanlage"), getSelf());
    }

    private void onMessage(String message) {
        log().info(message + " From Actor: " + getSender().path().toString());
    }

    public static Props props(ActorRef router) {
        return Props.create(ProducerRandomWorkActor.class, () -> new ProducerRandomWorkActor(router));
    }
}
