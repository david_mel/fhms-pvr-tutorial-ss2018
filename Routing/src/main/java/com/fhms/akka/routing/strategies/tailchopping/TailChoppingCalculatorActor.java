package com.fhms.akka.routing.strategies.tailchopping;

import com.fhms.akka.routing.helper.WorkHelper;

import akka.actor.AbstractLoggingActor;
import akka.actor.ActorRef;
import akka.actor.Props;

public class TailChoppingCalculatorActor extends AbstractLoggingActor {

	public static class AddMessage {
        private final int _number1;
        private final int _number2;

        public AddMessage(int number1, int number2){
            _number1=number1;
            _number2=number2;
        }
    }
	public static class ResultMessage {
        private final int result;
        private final String actorName;
        
		public ResultMessage(int result, String actorName) {
			super();
			this.result = result;
			this.actorName = actorName;
		}
		
		public int getResult() {
			return result;
		}

		public String getActorName() {
			return actorName;
		}
    }

    @Override
    public void preStart() throws Exception {
        super.preStart();

        log().info("CalculatorActor created");
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(AddMessage.class, this::onMessage)
                .build();
    }

    private void onMessage(AddMessage message) {
        int result = message._number1 + message._number2;

        String actorName = getSelf().path().name();
        
        log().info("Actor {} received the message to do the addition.", actorName);
        
        if(actorName.equals("$a") || actorName.equals("calculator1")) {
    		WorkHelper.spin(2000);
    	}    	
    	else if(actorName.equals("$b") || actorName.equals("calculator2")) {
    		WorkHelper.spin(100);
    	}    	
    	else {    		
    		WorkHelper.spin(2000);
    	}
        
        ActorRef sender = getSender();
        
        log().info("Send reply to {}", sender.path().name());
        
        sender.tell(new ResultMessage(result, actorName), getSelf());
    }
    public static Props props() {
        return Props.create(TailChoppingCalculatorActor.class);
    }

}
