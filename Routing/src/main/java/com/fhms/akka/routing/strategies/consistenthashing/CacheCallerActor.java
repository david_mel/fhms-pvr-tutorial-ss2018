package com.fhms.akka.routing.strategies.consistenthashing;

import akka.actor.AbstractLoggingActor;
import akka.actor.Props;

public class CacheCallerActor extends AbstractLoggingActor {
	
	public static Props props() {
		return Props.create(CacheCallerActor.class);
	}

	@Override
	public Receive createReceive() {
		return receiveBuilder()
				.match(String.class, this::onMessage)
				.build();
	}

	
	private void onMessage(String msg) {
		log().info(msg);
	}
}
