package com.fhms.akka.routing.actors;

import com.fhms.akka.routing.helper.WorkHelper;

import akka.actor.AbstractLoggingActor;
import akka.actor.Props;

public class CalculatorActor extends AbstractLoggingActor {
	public static class Message {
		private final double number1;
		private final double number2;

		public Message(double number1, double number2) {
			super();
			this.number1 = number1;
			this.number2 = number2;
		}

		public double getNumber1() {
			return number1;
		}

		public double getNumber2() {
			return number2;
		}
	}
	public static class AddMessage extends Message {
		public AddMessage(double _number1, double _number2) {
			super(_number1, _number2);
		}
	}
	public static class ResultMessage extends Message {
		private final double result;
		private final String actorName;

		public ResultMessage(double number1, double number2, double result, String actorName) {
			super(number1, number2);
			this.result = result;
			this.actorName = actorName;
		}

		public double getResult() {
			return result;
		}

		public String getActorName() {
			return actorName;
		}
	}

	public static Props props() {
		return Props.create(CalculatorActor.class);
	}

	@Override
	public Receive createReceive() {
		return receiveBuilder().match(AddMessage.class, this::onMessage).build();
	}

	private void onMessage(AddMessage msg) {
		log().info("Received {} #{}", msg.getClass().getName(), msg.getNumber1());

		double result = msg.getNumber1() + msg.getNumber2();

		ResultMessage resultMsg = new ResultMessage(msg.getNumber1(), msg.getNumber2(), result,
				getSelf().path().name());

		String actorName = getSelf().path().name();

		if(actorName.equals("$a") || actorName.equals("calculator1")) {
    		WorkHelper.spin(50);
    	}    	
    	else if(actorName.equals("$b") || actorName.equals("calculator2")) {
    		WorkHelper.spin(250);
    	}    	
    	else if(actorName.equals("$c")) {
    		WorkHelper.spin(1000);
    	}

		getSender().tell(resultMsg, getSelf());	
	}
}
