package com.fhms.akka.routing.strategies.tailchopping;

import akka.actor.AbstractLoggingActor;
import akka.actor.Props;

public class TailChoppingCallerActor extends AbstractLoggingActor {
    public static class Tick{}

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(TailChoppingCalculatorActor.ResultMessage.class, this::onMessage)
                .build();
    }

    private void onMessage(TailChoppingCalculatorActor.ResultMessage message) {
        log().info("Result: {}  from: {}", message.getResult(), message.getActorName());
    }

    public static Props props() {
        return Props.create(TailChoppingCallerActor.class);
    }
}
