package com.fhms.akka.routing.common;

import akka.actor.AbstractLoggingActor;
import akka.actor.Props;

import java.util.Random;

public class ConsumerRandomWorkActor extends AbstractLoggingActor {
    public static class Produce {
        private String _item;

        public Produce(String item) {
            _item = item;
        }

        public String getItem() {
            return _item;
        }
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(ConsumerRandomWorkActor.Produce.class, this::onMessage)
                .build();
    }

    private void onMessage(ConsumerRandomWorkActor.Produce message) {
        log().info("Producing item {}", message.getItem());

        Random rnd = new Random();

        long sum = 0;

        for (int i = 0; i < Math.abs(rnd.nextInt(100000)); i++) {
            sum += Math.abs(rnd.nextInt(10));
        }

        getSender().tell("Item " + message.getItem() + " with id " + sum + " produced!", getSelf());
    }

    public static Props props() {
        return Props.create(ConsumerRandomWorkActor.class);
    }
}
