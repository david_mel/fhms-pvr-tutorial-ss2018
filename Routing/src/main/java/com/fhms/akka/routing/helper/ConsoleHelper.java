package com.fhms.akka.routing.helper;

import java.util.Scanner;

public class ConsoleHelper {
	public static void waitForEnter() {
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);
		while (!scanner.hasNextLine()) {
			scanner.nextLine();
		}
	}
}
