package com.fhms.akka.routing.common;

import akka.actor.AbstractLoggingActor;
import akka.actor.Props;

public class ConsumerActor extends AbstractLoggingActor {
    public static class Produce {
        private String _item;

        public Produce(String item) {
            _item = item;
        }

        public String getItem() {
            return _item;
        }
    }

    @Override
    public Receive createReceive() {
        return receiveBuilder()
                .match(ConsumerActor.Produce.class, this::onMessage)
                .build();
    }

    private void onMessage(ConsumerActor.Produce message) {
        log().info("Producing item {}", message.getItem());

        // Random rnd = new Random();

       /* long sum = 0;

        for (int i = 0; i < 100000000; i++) {
            sum += Math.abs(rnd.nextInt(10));
        }*/

        // log().info("Item {} with id {} produced!", message.getItem(), sum);
    }

    public static Props props() {
        return Props.create(ConsumerActor.class);
    }
}
